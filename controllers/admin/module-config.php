<?php
abstract class LocalSystem_AdminModuleConfigController extends Controller
{
	static $module;
	static $customModel;
	static $title;
	static $fields;
	static $formWidth = 700;
	static $formFieldLabelWidth = '40%';
	static $useJsonSerialization = false;

	protected $form;
	private $config;
	private $options;

	function main($APP)
	{
		if (!self::$module)
			throw new Exception('The module property must be specified in the module config controller class');

		if (!self::$fields)
			throw new Exception('The fields property must be specified in the module config controller class');

		if (!self::$title)
			self::$title = _T('Configuration');

		$this->setTitle(self::$title);

		$this->loadConfig();

		if ($this->options) {
			// Populate form with current config options
			foreach ($this->options as $option => $value) {
				if (self::$fields[$option])
					self::$fields[$option]['default'] = $value;
			}
		}

		if (self::$customModel) {
			$this->form = new Form(self::$customModel, _T('configuration'), 1, Form::MODE_UPDATE);
		} else {
			$this->form = new Form('module_config', _T('configuration'), null, 'custom');
			$this->form->onSubmit([$this, 'onSubmit']);
		}

		$this->form
			->configure([
				'width' => self::$formWidth,
				'FIELD_LABEL_WIDTH' => self::$formFieldLabelWidth,
				'langAltMode' => true
			])
			->addFields(self::$fields);
	}

	public function loadConfig()
	{
		$configKeyName = System\Config::getOptionsConfigKeyName();

		if (!($config = module_config::findOne(['module' => self::$module, 'key' => $configKeyName]))) {
			$config = new module_config([
				'module' => self::$module,
				'key' => $configKeyName,
				'value' => $this->serialize((object) [])
			]);

			$config->save();
		}

		$this->config = $config;
		$this->options = $this->unserialize($this->config->value);
	}

	public function getOptions()
	{
		return $this->options;
	}

	function onSubmit($data)
	{
		$configKeyName = System\Config::getOptionsConfigKeyName();

		$this->config->set([
			'key' => $configKeyName,
			'value' => $this->serialize($data)
		]);

		$this->config->save();

		$this->message(_T('The configuration has been saved.'));

		$this->onUpdate($this->options);
	}

	private function serialize($data)
	{
		if (static::$useJsonSerialization) {
			return json_encode($data);
		} else {
			return serialize($data);
		}
	}

	private function unserialize($data)
	{
		if (static::$useJsonSerialization) {
			return json_decode($data, true);
		} else {
			return unserialize($data);
		}

	}

	function onUpdate($options)
	{

	}

	function view()
	{
		$this->form->view();
	}
}