--
-- Table structure for table `module_config`
--

CREATE TABLE `module_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
