<?php
abstract class ModuleConfig {
    private static function getModuleConfig($moduleName){
        $configKeyName = System\Config::getOptionsConfigKeyName();

        if(!($moduleConfig = module_config::findOne(['module' => $moduleName, 'key' => $configKeyName]))){
            $moduleConfig = new module_config([
                'module' => $moduleName,
                'key' => $configKeyName,
                'value' => serialize((object) [])
            ]);

            $moduleConfig->save();
        }

        return $moduleConfig;
    }

    static function get(){
        $moduleConfig = static::getModuleConfig(static::$moduleName);

        $options = unserialize($moduleConfig->value);

        $config = new static();

        $config->set($options);

        return $config;
    }

    function set($values){
        foreach($values as $key => $value){
            $this->{$key} = $value;
        }
    }

    function save(){
        $moduleConfig = static::getModuleConfig(static::$moduleName);

        $data = new stdClass;

        foreach($this as $field => $value){
            $data->{$field} = $value;
        }

        $moduleConfig->value = serialize($data);

        $moduleConfig->save();

        return true;
    }
}