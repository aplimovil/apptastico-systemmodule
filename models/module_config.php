<?php
class module_config extends Model {
	use Uploads, ImageResizer;

	public $id;
	public $module;
	public $key;
	public $value;
}