<?php
namespace System;

class Util {
	static $dateFormat = 'd/m/Y';
	static $dateTimeFormat = 'd/m/Y g:i a';

	static function formatDate($date){
		$time = strtotime($date);
		return $time ? date(static::$dateFormat, $time) : '';
	}

	static function formatDateTime($date){
		$time = strtotime($date);
		return $time ? date(static::$dateTimeFormat, $time) : '';
	}

	static function formatMoney($amount, $decimals = 2, $symbol = '$'){
		return $symbol . number_format($amount, $decimals);
	}
}