<?php
namespace System;

abstract class Config {
	static $moduleName = '';
	static $defaultOptions = [];

	protected static $_options = [];
	
	private static $_optionsConfigKeyName = 'options';

	static function loadOptions(){
		if(!static::$moduleName)
			throw new \Exception('moduleName property must be defined in config class.');

		if(!static::$_options[static::$moduleName]){
			$configKeyName = self::$_optionsConfigKeyName;

			if(!($config = \module_config::findOne(['module' => static::$moduleName, 'key' => $configKeyName]))){
				$config = new \module_config([
					'module' => static::$moduleName,
					'key' => $configKeyName,
					'value' => serialize((object) [])
				]);
			}

			static::$_options[static::$moduleName] = unserialize($config->value);
		}
	}

	static function getOptionsConfigKeyName(){
		return static::$_optionsConfigKeyName;
	}

	static function getOption($name){
		static::loadOptions();

		$value = static::$_options[static::$moduleName]->{$name};

		if($value == '' && static::$defaultOptions[$name]){
			$value = static::$defaultOptions[$name];
		}

		return $value;
	}
}